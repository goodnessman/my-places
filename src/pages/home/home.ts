import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';

// Custom

// services
import { PlacesService } from '../../services/places.service';
// pages
import { NewPlacePage } from '../new-place/new-place';
import { PlacePage } from '../place/place';
// models
import { Place } from '../../model/place.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	places: Place[] = [];

  constructor(
  	public navCtrl: NavController, 
  	private placesService: PlacesService,
  	private modalCtrl: ModalController) {}

  ionViewWillEnter() {
  	this.placesService.getPlaces().then(
  		(places) => this.places = places
  		);
  }

  onLoadNewPlace() {
  	this.navCtrl.push(NewPlacePage);
  }

  onOpenPlace(place: Place) {
  	this.modalCtrl.create(PlacePage, place).present();
  }
}
