
* Get Ionic DevApp for easy device testing: https://bit.ly/ionic-dev-app
* Finish setting up Ionic Pro Error Monitoring: https://ionicframework.com/docs/pro/monitoring/#getting-started
* Make sure you've added your git host as the origin remote: git remote add origin https://github.com/nikadrenalin/my-places.git
* Finally, push your code to perform real-time updates, and more: git push


Version: 1.0.0 MY-PLACES:ADD Finish project

Application versions: 1.2.3
    develop/master
	1 - major versions;
	2 - release version;
	3 - local changes;

start: ionic serve